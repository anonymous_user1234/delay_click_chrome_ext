# delay click chrome extension

adds right-click option which will queue to click the target in a certain time delay. The delay range can be adjusted.

This extension is not uploaded to chrome extension store. Therefore the user shall have to manually install it.

**Latest version: v0.3**

## installation

- download this repo. git clone or zip download, it doesn't matter.
- open chrome
- goto `chrome://extensions` in the address bar
- on the top-left, click `Load unpacked`
- open the folder where this repo has been cloned/unzipped.
- the extension should be installed in no time.

## tested environment

tested with Chrome: Version 71.0.3578.98 (Official Build) (64-bit)

## License

See the [LICENSE](LICENSE.md) file for license rights and limitations (MIT).
