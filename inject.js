// Copyright 2018 udontneedtoknow524@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//   http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var clickedEl = null;
var url_element_array=[]
var id_prefix = Math.random().toString(36).substr(2, 5)
var id_count=0


document.addEventListener("mousedown", function(event){
    //right click
    if(event.button == 2) { 
        
        clickedEl = event.target;

        if(clickedEl.id==null || clickedEl.id==""){
            clickedEl.id=id_prefix+id_count
            id_count++
        }
        
        // console.log(event.target)

    }
}, true);

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if(request.id == "trigger_delay_click") {

        var naver_news_highlight = request.naver_news_highlight
        
        if(naver_news_highlight){
            var candidate = document.getElementById(clickedEl.id).closest(".u_cbox_area")
            if(candidate!=null){
                candidate.style.backgroundColor="pink"
            }
        }
        
        

        url_element_array.push(clickedEl)
        var pushed_item_id=url_element_array.length-1
        sendResponse({value: pushed_item_id});
    }

    if(request.id=="test1"){
        var fetch_id = request.click_id

        var fetched_element = url_element_array[fetch_id]
        sendResponse({click_id: fetch_id})

        fetched_element.click()

    }
});