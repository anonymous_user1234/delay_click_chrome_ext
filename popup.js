// Copyright 2018 udontneedtoknow524@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//   http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


var port = chrome.runtime.connect({
    name: "Sample Communication"
});

var empty_message_element = $("#empty_message")
var waitlist = $("#waitlist")
var viewdocument = null


var views = chrome.extension.getViews({
    type: "popup"
});

viewdocument = views[0].document



port.postMessage({
    "type": "initmsg"
});

port.onMessage.addListener(function (msg) {
    chrome.extension.getBackgroundPage().console.log("message recieved from popup.js side")
    chrome.extension.getBackgroundPage().console.log(msg)


    if (msg.type == "progress_info") {



        viewdocument.getElementById("launched_count_span").textContent = msg.data.length
        var finished_count = 0

        if (msg.data.length == 0) {
            chrome.extension.getBackgroundPage().console.log("appending")

            var textnode = viewdocument.createTextNode("no items to show")

            empty_message_element.append($('<p>no items to show</p>'))

        }
        else {

            empty_message_element.css("visibility", "hidden")

            waitlist.empty()

            chrome.extension.getBackgroundPage().console.log("inside msg.data.length not zero")
            var i = 0;

            for (i = 0; i < msg.data.length; i++) {
                var data = msg.data[i]
                // chrome.extension.getBackgroundPage().console.log(data)
                if (data.finished) {
                    finished_count++
                }

                var newitem = $('<div>', { class: 'list-group-item clearfix' })
                var description = $('<p>', { class: 'float-left' })
                description.html(data.tabid + "-" + data.click_id)

                var statusbtn;
                if (data.finished) {

                    if(data.finish_state=="SUCCESS"){
                        statusbtn = $('<button>', { class: "btn btn-success float-right" })
                        statusbtn.prop("disabled", "true")
                        statusbtn.html("done")
                    }
                    else if(data.finish_state=="FAILED"){
                        statusbtn = $('<button>', { class: "btn btn-danger float-right" })
                        statusbtn.prop("disabled", "true")
                        statusbtn.html("fail")
                    }
                    else{
                        chrome.extension.getBackgroundPage().console.log("unrecognized data.finish_state: "+ data.finish_state)
                    }
                    
                }
                else {
                    statusbtn = $('<button>', { class: "btn btn-secondary float-right" })
                    statusbtn.prop("disabled", "true")
                    statusbtn.html("waiting")
                }

                newitem.append(description)
                newitem.append(statusbtn)


                waitlist.append(newitem)


            }
        }

        viewdocument.getElementById("finished_count_span").textContent = finished_count

    }

    if (msg.type == "get_config") {


        var data = msg.data
        var min_delay = data.min_delay
        var max_delay = data.max_delay
        var naver_news_highlight = data.naver_news_highlight

        chrome.extension.getBackgroundPage().console.log("naver_news_highlight:"+naver_news_highlight)

        $("#min_delay_input").val(min_delay)
        $("#max_delay_input").val(max_delay)
        
        $("#naver_news_highlight_checkbox").prop("checked",naver_news_highlight)
        
        // $("#naver_news_highlight_checkbox").val(naver_news_highlight)
    }

});


$("#config_btn").click(function (e) {
    chrome.extension.getBackgroundPage().console.log("config btn clicked")

    port.postMessage({
        "type": "get_config"
    });
})


function config_validation(){
    var min_delay_val = $("#min_delay_input").val()
    var max_delay_val = $("#max_delay_input").val()

    var false_flag=false

    if(min_delay_val<0){
        $("#min_delay_input").addClass("is-invalid")
        false_flag = true
    }

    if(max_delay_val<0){
        $("#max_delay_input").addClass("is-invalid")
        false_flag = true
    }

    if(min_delay_val >= max_delay_val){

        $("#min_delay_input").addClass("is-invalid")
        false_flag = true
    }

    return !false_flag

}


function restore_invalids(){
    $("#min_delay_input").removeClass("is-invalid")
    $("#max_delay_input").removeClass("is-invalid")

}


$("#modal_save_btn").click(function(e){


    var check_result = config_validation()

    if(!check_result){
        return
    }

    // restore invalid stuff of configs
    restore_invalids()



    var min_delay_val = $("#min_delay_input").val()
    var max_delay_val = $("#max_delay_input").val()
    var naver_news_highlight = $("#naver_news_highlight_checkbox").is(":checked")

    console.log("saving... naver_news_highlight"+ naver_news_highlight)


    chrome.extension.getBackgroundPage().console.log("save change btn clicked")
    chrome.extension.getBackgroundPage().console.log(min_delay_val, max_delay_val)


    port.postMessage({
        "type": "update_config",
        "data":{
            "min_delay": min_delay_val,
            "max_delay": max_delay_val,
            "naver_news_highlight": naver_news_highlight
        }
    })

    $("#exampleModal").modal('hide')
})