// Copyright 2018 udontneedtoknow524@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//   http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


chrome.runtime.onInstalled.addListener(function () {
    // Create one test item for each context type.
    var context = "all";
    var title = "add to delayed click";
    var id = chrome.contextMenus.create({ "title": title, "contexts": [context], "id": "context" + context });

});


chrome.contextMenus.onClicked.addListener(onClickHandler);


var last_trigger_time = -1
var global_port = null


var min_delay = 12000
var max_delay = 20000
var naver_news_highlight = true

function get_delay() {
    var next_trigger_time = 0;
    var d = new Date()
    var now = d.getTime()

    if (last_trigger_time <= now) {
        last_trigger_time = now
    }

    if (last_trigger_time < 0) {
        // first delay will occur in 2seconds
        next_trigger_time = now + 2000
    }
    else {
        
        var delay_delta = max_delay - min_delay

        random_addition = parseInt(Math.random() * delay_delta)
        next_trigger_time = parseInt(last_trigger_time) + parseInt(min_delay) + parseInt(random_addition)
    }

    var return_delay = next_trigger_time - parseInt(now)
    last_trigger_time = next_trigger_time
    return return_delay
}

var progress_queue = []

function onClickHandler(info, tab) {

    var sendmsg={
        "id": "trigger_delay_click",
        "naver_news_highlight": naver_news_highlight
    }

    chrome.tabs.sendMessage(tab.id, sendmsg, function (clickedEl) {

        // elt.value = clickedEl.value;
        chrome.extension.getBackgroundPage().console.log("clickedEl:")
        chrome.extension.getBackgroundPage().console.log(clickedEl)
        
        var tabid = tab.id

        var delay = get_delay()
        chrome.extension.getBackgroundPage().console.log("getdelay:"+ delay)

        var click_id = clickedEl.value

        var dcitem = new DelayClickItem(tabid, click_id, false)
        progress_queue.push(dcitem)

        // console.log("showing progress_queue")
        // console.log(progress_queue)

        setTimeout(function () {
            var msg = {
                "id": "test1",
                "click_id": clickedEl.value
            }
            // console.log("timeout done. executing function with tabid=" + tabid)
            chrome.tabs.sendMessage(tabid, msg, function (data) {
                // this is the callback function.
                console.log("background test1 rsp: received data")
                console.log(data)
                var dcitem
                if(data==null){
                    dcitem = fetch_dcitem(tabid, clickedEl.value)  
                    
                    if(dcitem!=null){
                        dcitem.finished=true
                        dcitem.finish_state = "FAILED"
                    }
                }
                else{
                    dcitem = fetch_dcitem(tabid, data.click_id)

                    if(dcitem!=null){
                        dcitem.finished=true
                        dcitem.finish_state = "SUCCESS"
                    }
                }


                if (dcitem == null) {
                    console.log("cannot retrieve dcitem")
                }
                

                update_progress_to_popup()
            })

        }, delay)

    });


};


class DelayClickItem {
    constructor(tabid, click_id, finished) {
        this.tabid = tabid
        this.click_id = click_id
        this.finished = finished
        this.finish_state=null
    }
}

function fetch_dcitem(tabid, click_id) {
    var i;
    for (i = 0; i < progress_queue.length; i++) {
        var temp_dcitem = progress_queue[i]
        if (temp_dcitem.tabid == tabid && temp_dcitem.click_id == click_id) {
            return temp_dcitem
        }

    }
    return null
}


function update_progress_to_popup() {
    console.log("inside update_progress_to_popup")

    // send update progress to popup only when global_port is valid
    if (global_port != null) {

        console.log(global_port)

        var sendmsg = {
            "type": "progress_info",
            "data": progress_queue
        }
        global_port.postMessage(sendmsg)
    }

}

function return_config_to_popup() {
    if (global_port != null) {
        var sendmsg = {
            "type": "get_config",
            "data": {
                "min_delay": min_delay,
                "max_delay": max_delay,
                "naver_news_highlight": naver_news_highlight

            }
        }

        global_port.postMessage(sendmsg)
    }
}


function update_config(data){
    if(data.min_delay!=null){
        min_delay = data.min_delay
    }

    if(data.max_delay !=null){
        max_delay = data.max_delay
    }

    if(data.naver_news_highlight !=null){
        naver_news_highlight = data.naver_news_highlight
    }

    console.log("updated min/max delay. min_delay="+min_delay +", max_delay="+ max_delay)
}

chrome.extension.onConnect.addListener(function (port) {
    global_port = port
    console.log("Connected .....");
    port.onMessage.addListener(function (msg) {

        console.log(msg)


        if (msg.type == "initmsg") {

            update_progress_to_popup()
        }

        if (msg.type == "get_config") {
            return_config_to_popup()
        }

        if(msg.type =="update_config"){
            update_config(msg.data)
        }
    })

    port.onDisconnect.addListener(function(){
        console.log("port disconnected. here is the lastError")
        console.log(chrome.runtime.lastError)
        global_port=null

    })
})



